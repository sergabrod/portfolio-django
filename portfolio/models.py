from django.db import models

# Create your models here.
class Project(models.Model):
    title = models.CharField(max_length=200,verbose_name='Título')
    description = models.TextField(verbose_name='Descripción')
    image = models.ImageField(verbose_name='Imágen', upload_to='projects')
    link = models.URLField(verbose_name='enlace', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')

    # Clase de metadata donde configuramos los nombres en español
    class Meta:
        verbose_name = 'proyecto'
        verbose_name_plural = 'proyectos'
        # Ordenamos por el campo created, con el "-" primero apareceran los más nuevos
        ordering = ['-created']

    # Redefinimos el método __str__ para que muestre el titulo del proyecto
    def __str__(self):
        return self.title